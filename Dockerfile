# Redis Container Image
FROM redis:6.2.13-alpine3.18

# Usually runs on 6379
EXPOSE 6379

HEALTHCHECK CMD curl --fail http://localhost:6379 || exit 1
USER alejandro
CMD ["redis-server", "--port", "6379"]
